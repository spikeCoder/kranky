import numpy as np
import nidaqmx




class NiDaqmxTaskWrapper(object):
    def __init__(self, params):
        self.range_daq=10
        system = nidaqmx.System()
        dev = system.devices[0]
        channels = dev.get_analog_output_channels()
        if len(channels) < params['n_ao_channels']:
            raise Exception('More channels requested than exist on device.')
        self.task = nidaqmx.AnalogOutputTask()
        for k in range(0,params['n_ao_channels']):
            self.task.create_voltage_channel(channels[k], min_val = -self.range_daq, max_val=self.range_daq)
        self.task.configure_timing_sample_clock(rate=params['ao_freq'])
        self.dtype_out = np.dtype(np.int16)
        self.maxvalue_data = 2**(8*self.dtype_out.itemsize-1)-1
        pass

    def write(self,chunk):
        chunk = np.fromstring(chunk, self.dtype_out)
        chunk = chunk.astype(np.float64)
        chunk = (chunk/self.maxvalue_data)*(self.range_daq*0.99)
        self.task.write(chunk, auto_start = True)
        pass

