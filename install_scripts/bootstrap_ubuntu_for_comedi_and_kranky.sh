# install all apt-get python requirments

# install existing packages
APTPACKAGES="python-scipy cython python-pip"
PIPPACKAGES="bitarray"
sudo apt-get update
for PACK in $APTPACKAGES
do
    sudo apt-get --assume-yes install $PACK
done
for PACK in $PIPPACKAGES
do
    sudo pip install $PACK --upgrade
done

# install custom pycomedi
CD = $(pwd)
cd lib/pycomedi/
sudo python setup.py install
cd $CD 

